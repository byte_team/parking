﻿//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2014 - 2017 BoneCracker Games
// http://www.bonecrackergames.com
// Buğra Özdoğanlar
//
//----------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Выбор цвета с помощью ползунков пользовательского интерфейса.
/// </summary>
[AddComponentMenu("BoneCracker Games/Realistic Car Controller/UI/RCC Color Picker By UI Sliders")]
public class RCC_ColorPickerBySliders : MonoBehaviour {

	public Color color;     // Основной цвет.

    // Слайдеры на цветовой канал.
    public Slider redSlider;
	public Slider greenSlider;
	public Slider blueSlider;

	public void Update () {

        // Назначение нового цвета основному цвету.
        color = new Color (redSlider.value, greenSlider.value, blueSlider.value);
	
	}

}
