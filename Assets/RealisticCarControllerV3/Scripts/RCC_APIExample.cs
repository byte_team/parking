﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
/// Пример скрипта, демонстрирующий работу RCC API.
///</summary>
public class RCC_APIExample : MonoBehaviour {

	public RCC_CarControllerV3 spawnVehiclePrefab;          // Автомобиль префаб, на котором мы собираемся появиться.
    private RCC_CarControllerV3 currentVehiclePrefab;		//Спаун авто
	public Transform spawnTransform;								// Spawn transform.

	public bool playerVehicle;          // Спаун как транспортное средство игрока?
    public bool controllable;           // Спаун как управляемый автомобиль?
    public bool engineRunning;      // Спаун с работающим двигателем?

    public void Spawn(){

        // Спаун автомобиля с заданными настройками.
        currentVehiclePrefab = RCC.SpawnRCC (spawnVehiclePrefab, spawnTransform.position, spawnTransform.rotation, playerVehicle, controllable, engineRunning);

	}

	public void SetPlayer(){

        // Регистрирует транспортное средство как транспортное средство игрока.
        RCC.RegisterPlayerVehicle (currentVehiclePrefab);

	}

	public void SetControl(bool control){

        // Включает / отключает управляемое состояние автомобиля.
        RCC.SetControl (currentVehiclePrefab, control);

	}

	public void SetEngine(bool engine){

        // Запускает / отключает двигатель автомобиля.
        RCC.SetEngine (currentVehiclePrefab, engine);

	}

	public void DeRegisterPlayer(){

        // Отмена регистрации транспортного средства в качестве транспортного средства игрока.
        RCC.DeRegisterPlayerVehicle ();

	}

}
