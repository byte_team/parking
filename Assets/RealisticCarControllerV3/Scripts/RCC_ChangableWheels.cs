﻿//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2014 - 2017 BoneCracker Games
// http://www.bonecrackergames.com
// Buğra Özdoğanlar
//
//----------------------------------------------

using UnityEngine;
using System.Collections;

/// <summary>
/// Меняет колеса (только визуально) во время выполнения. Это держит сменные колеса как сборный в массиве.
/// </summary>
[System.Serializable]
public class RCC_ChangableWheels : ScriptableObject {
	
	#region singleton
	private static RCC_ChangableWheels instance;
	public static RCC_ChangableWheels Instance{	get{if(instance == null) instance = Resources.Load("RCC Assets/RCC_ChangableWheels") as RCC_ChangableWheels; return instance;}}
	#endregion

	[System.Serializable]
	public class ChangableWheels{
		
		public GameObject wheel;

	}
		
	public ChangableWheels[] wheels;

}


