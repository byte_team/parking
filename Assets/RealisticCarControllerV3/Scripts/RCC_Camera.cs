﻿//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2014 - 2017 BoneCracker Games
// http://www.bonecrackergames.com
// Buğra Özdoğanlar
//
//----------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

/// <summary>
/// Главный контроллер камеры RCC. Включает в себя 6 различных режимов камеры с множеством настраиваемых настроек. Он не использует разные камеры на вашей сцене, как * другие * активы. Просто он привязывает камеру к своим позициям, вот и все. Не нужно быть Эйнштейном.
/// Также поддерживает обнаружение столкновений для этой новой версии (V3.2).
/// </summary>
[AddComponentMenu("BoneCracker Games/Realistic Car Controller/Camera/RCC Camera")]
public class RCC_Camera : MonoBehaviour{

    // Получение экземпляра основных общих настроек RCC.
    #region RCC Settings Instance

    private RCC_Settings RCCSettingsInstance;
	private RCC_Settings RCCSettings {
		get {
			if (RCCSettingsInstance == null) {
				RCCSettingsInstance = RCC_Settings.Instance;
			}
			return RCCSettingsInstance;
		}
	}

    #endregion

    // В настоящее время рендеринг?
    public bool isRendering = true;

    // Цель, которой мы следуем за transform и hardbody.
    public RCC_CarControllerV3 playerCar;
	private Rigidbody playerRigid;
	private float playerSpeed = 0f;
	private Vector3 playerVelocity = new Vector3 (0f, 0f, 0f);

	public Camera thisCam;          // Камера не прикреплена к этому основному игровому объекту. Камера является родителем игрового объекта. Поэтому мы можем применить дополнительные изменения положения и поворота.
    public GameObject pivot;        // Поворотный центр камеры. Используется для создания смещений и столкновений.

    // Режимы камеры.
    public CameraMode cameraMode;
	public enum CameraMode{TPS, FPS, WHEEL, FIXED, CINEMATIC, TOP}
	public CameraMode lastCameraMode;

	public bool useTopCameraMode = false;               // Должны ли мы использовать режим верхней камеры?
    public bool useHoodCameraMode = true;               // Должны ли мы использовать режим камеры с капотом?
    public bool useOrbitInTPSCameraMode = false;    // Должны ли мы использовать управление орбитой в режиме камеры TPS?
    public bool useOrbitInHoodCameraMode = false;   // Должны ли мы использовать управление орбитой в режиме камеры капота?
    public bool useWheelCameraMode = true;              // Должны ли мы использовать режим камеры колеса?
    public bool useFixedCameraMode = false;             // Должны ли мы использовать фиксированный режим камеры?
    public bool useCinematicCameraMode = false;     // Должны ли мы использовать кинематографический режим камеры?
    public bool useOrthoForTopCamera = false;           // Должны ли мы использовать орто в режиме верхней камеры?
    public bool useOcclusion = true;                            // Должны ли мы использовать окклюзию камеры?
    public LayerMask occlusionLayerMask = -1;

	public bool useAutoChangeCamera = false;            // Должны ли мы изменить режим камеры автоматически?
    private float autoChangeCameraTimer = 0f;

	public Vector3 topCameraAngle = new Vector3(45f, 45f, 0f);      // Если это так, мы будем использовать этот угол Vector3 для режима верхней камеры.

    private float distanceOffset = 0f;	
	public float maximumZDistanceOffset = 10f;      // Смещение расстояния для режима верхней камеры. Связано со скоростью автомобиля. Если скорость автомобиля выше, камера переместится к передней части автомобиля.
    public float topCameraDistance = 100f;              // Смещение расстояния для режима верхней камеры. Связано со скоростью автомобиля. Если скорость автомобиля выше, камера переместится к передней части автомобиля.

    // Используется для плавных движений камеры. Плавные движения камеры используются только для режимов TPS и Top camera.
    private Vector3 targetPosition, lastFollowerPosition = Vector3.zero;
	private Vector3 lastTargetPosition = Vector3.zero;

    // Используется для сброса значений орбиты, когда направление транспортного средства было изменено.
    private int direction = 1;
	private int lastDirection = 1;

	public float TPSDistance = 6f;              // Расстояние для режима камеры TPS.
    public float TPSHeight = 2f;                    // Высота, которую мы хотим, чтобы камера находилась выше цели для режима камеры TPS.
    public float TPSHeightDamping = 10f;    // Демпфер движения по высоте.
    public float TPSRotationDamping = 5f;   // Демпфер вращения.
    public float TPSTiltMaximum = 15f;      // Максимальный угол наклона, связанный с локальной скоростью твердого тела.
    public float TPSTiltMultiplier = 2f;        // Множитель угла наклона.
    private float TPSTiltAngle = 0f;            // Текущий угол наклона.
    public float TPSYawAngle = 0f;          // Угол рыскания.
    public float TPSPitchAngle = 7f;            // Угол тангажа.

    internal float targetFieldOfView = 60f; // Камера адаптирует свое поле зрения к этому целевому полю зрения. Все поля зрения ниже этой линии будут кормить это значение.

    public float TPSMinimumFOV = 50f;           // Минимальное поле зрения, связанное со скоростью автомобиля.
    public float TPSMaximumFOV = 70f;           // Максимальное поле зрения, связанное со скоростью автомобиля.
    public float hoodCameraFOV = 60f;           // Капот поля зрения.
    public float wheelCameraFOV = 60f;          // Колесо поля зрения.
    public float minimumOrtSize = 10f;          // Минимальный размер орто, связанный со скоростью транспортного средства.
    public float maximumOrtSize = 20f;          // Максимальный размер орто, связанный со скоростью транспортного средства.

    internal int cameraSwitchCount = 0;                 // Используется в переключателе для запуска соответствующего метода режима камеры.
    private RCC_HoodCamera hoodCam;                 // Капот камеры. Это нулевой сценарий. Просто используется для нахождения капота камеры, отождествленной с нашим плеером.
    private RCC_WheelCamera wheelCam;               // Колесо камеры. Это нулевой сценарий. Просто используется для нахождения колесной камеры, родственной нашему плееру.
    private RCC_FixedCamera fixedCam;                   // Исправлена система камер.
    private RCC_CinematicCamera cinematicCam;       // Кинематографическая система камер.

    private Vector3 collisionVector = Vector3.zero;             // Вектор столкновения.
    private Vector3 collisionPos = Vector3.zero;                    // Позиция столкновения.
    private Quaternion collisionRot = Quaternion.identity;  // Вращение столкновения.

    private float index = 0f;               // Используется для эффекта синусового поля зрения после тяжелых сбоев.

    private Quaternion orbitRotation = Quaternion.identity;     // Вращение орбиты.

    // Орбита X и Y входов.
    internal float orbitX = 0f;
	internal float orbitY = 0f;

    // Минимальная и максимальная орбита X, Y градусов.
    public float minOrbitY = -20f;
	public float maxOrbitY = 80f;

    // Орбита X и Y скоростей.
    public float orbitXSpeed = 7.5f;
	public float orbitYSpeed = 5f;
	private float orbitResetTimer = 0f;

    // Рассчитать текущие углы поворота для режима TPS.
    private Quaternion currentRotation = Quaternion.identity;
	private Quaternion wantedRotation = Quaternion.identity;
	private float currentHeight = 0f;
	private float wantedHeight = 0f;

	public delegate void onBCGCameraSpawned(GameObject BCGCamera);
	public static event onBCGCameraSpawned OnBCGCameraSpawned;

	void Awake(){

        // Получение камеры.
        thisCam = GetComponentInChildren<Camera>();

        // Правильные настройки для выбранного типа поведения.
        switch (RCCSettings.behaviorType){

		case RCC_Settings.BehaviorType.SemiArcade:
			break;

		case RCC_Settings.BehaviorType.Drift:
			break;

		case RCC_Settings.BehaviorType.Fun:
			break;

		case RCC_Settings.BehaviorType.Racing:
			break;

		case RCC_Settings.BehaviorType.Simulator:
			break;

		}

	}

	void OnEnable(){

        // Вызываем это событие при появлении камеры BCG.
        if (OnBCGCameraSpawned != null)
			OnBCGCameraSpawned (gameObject);

		RCC_CarControllerV3.OnRCCPlayerCollision += RCC_CarControllerV3_OnRCCPlayerCollision;

        // Прошлые позиции, используемые для правильного сглаживания, связанные со скоростью.
        lastFollowerPosition = transform.position;
		lastTargetPosition = transform.position;

	}

	void RCC_CarControllerV3_OnRCCPlayerCollision (RCC_CarControllerV3 RCC, Collision collision){

		Collision (collision);
		
	}

	void GetTarget(){

        // Возвращаемся, если у нас нет машины игрока.
        if (!playerCar)
			return;

        // Если у транспортного средства игрока есть RCC_CameraConfig, расстояние и высота будут скорректированы.
        if (playerCar.GetComponent<RCC_CameraConfig> ()) {

			TPSDistance = playerCar.GetComponent<RCC_CameraConfig> ().distance;
			TPSHeight = playerCar.GetComponent<RCC_CameraConfig> ().height;

		}

        // Становимся жёсткими из машины игрока.
        playerRigid = playerCar.GetComponent<Rigidbody>();

        // Получение режимов камеры от транспортного средства игрока.
        hoodCam = playerCar.GetComponentInChildren<RCC_HoodCamera>();
		wheelCam = playerCar.GetComponentInChildren<RCC_WheelCamera>();
		fixedCam = GameObject.FindObjectOfType<RCC_FixedCamera>();
		cinematicCam = GameObject.FindObjectOfType<RCC_CinematicCamera>();

		ResetCamera ();

        // Установка трансформации и положения транспортного средства игрока при переключении цели камеры.
        //		transform.position = playerCar.transform.position;
        //		transform.rotation = playerCar.transform.rotation * Quaternion.AngleAxis(10f, Vector3.right);

    }

    public void SetTarget(GameObject player){

		playerCar = player.GetComponent<RCC_CarControllerV3>();
		GetTarget ();

	}

	public void RemoveTarget(){

		transform.SetParent (null);

		playerCar = null;
		playerRigid = null;

	}

	void Update(){

        // Если он активен, включите камеру. Если это не так, отключите камеру.
        if (!isRendering) {

			if(thisCam.gameObject.activeInHierarchy)
				thisCam.gameObject.SetActive (false);

			return;

		} else {

			if(!thisCam.gameObject.activeInHierarchy)
				thisCam.gameObject.SetActive (true);

		}

        // Рано, если у нас нет машины игрока.
        if (!playerCar || !playerRigid){

			GetTarget();
			return;

		}

        // Скорость автомобиля (сглаженная).
        playerSpeed = Mathf.Lerp(playerSpeed, playerCar.speed, Time.deltaTime * 5f);

        // Скорость автомобиля.
        playerVelocity = playerCar.transform.InverseTransformDirection(playerRigid.velocity);

        // Используется для эффекта синусового поля зрения после тяжелых сбоев. 
        if (index > 0)
			index -= Time.deltaTime * 5f;

        // Привязываем текущее поле зрения к целевому полю зрения.
        thisCam.fieldOfView = Mathf.Lerp (thisCam.fieldOfView, targetFieldOfView, Time.deltaTime * 5f);

	}

	void LateUpdate (){

        // Рано, если у нас нет машины игрока.
        if (!playerCar || !playerRigid)
			return;

        // Даже если у нас есть машина игрока и она отключена, вернитесь.
        if (!playerCar.gameObject.activeSelf)
			return;

        // Запустить соответствующий метод с выбранным режимом камеры.
        switch (cameraMode){

		case CameraMode.TPS:
			TPS();
			if (useOrbitInTPSCameraMode)
				ORBIT ();
			break;

		case CameraMode.FPS:
			FPS ();
			if (useOrbitInHoodCameraMode)
				ORBIT ();
			break;

		case CameraMode.WHEEL:
			WHEEL();
			break;

		case CameraMode.FIXED:
			FIXED();
			break;

		case CameraMode.CINEMATIC:
			CINEMATIC();
			break;

		case CameraMode.TOP:
			TOP();
			break;

		}

		if (lastCameraMode != cameraMode)
			ResetCamera ();

		lastCameraMode = cameraMode;
		autoChangeCameraTimer += Time.deltaTime;

		if(Input.GetKeyDown(RCCSettings.changeCameraKB))
			ChangeCamera();

		if (useAutoChangeCamera && autoChangeCameraTimer > 10) {

			autoChangeCameraTimer = 0f;
			ChangeCamera ();

		}

	}

    // Изменить камеру путем увеличения счетчика переключателей камеры.
    public void ChangeCamera(){

        // Увеличение счетчика переключателей камеры при каждой смене камеры.
        cameraSwitchCount++;

        // Всего у нас 6 режимов камеры. Если счетчик переключателя камеры больше максимального, установите его на 0.
        if (cameraSwitchCount >= 6)
			cameraSwitchCount = 0;

		switch(cameraSwitchCount){

		case 0:
			cameraMode = CameraMode.TPS;
			break;

		case 1:
			if(useHoodCameraMode && hoodCam){
				cameraMode = CameraMode.FPS;
			}else{
				ChangeCamera();
			}
			break;

		case 2:
			if(useWheelCameraMode && wheelCam){
				cameraMode = CameraMode.WHEEL;
			}else{
				ChangeCamera();
			}
			break;

		case 3:
			if(useFixedCameraMode && fixedCam){
				cameraMode = CameraMode.FIXED;
			}else{
				ChangeCamera();
			}
			break;

		case 4:
			if(useCinematicCameraMode && cinematicCam){
				cameraMode = CameraMode.CINEMATIC;
			}else{
				ChangeCamera();
			}
			break;

		case 5:
			if(useTopCameraMode){
				cameraMode = CameraMode.TOP;
			}else{
				ChangeCamera();
			}
			break;

		}

	}

    // Изменить камеру, установив ее в конкретный режим.
    public void ChangeCamera(CameraMode mode){

		cameraMode = mode;

	}

	void FPS(){

        // Назначение вращения орбиты и преобразование вращения.
        transform.rotation = Quaternion.Lerp(transform.rotation, playerCar.transform.rotation * orbitRotation, Time.deltaTime * 5f);
		thisCam.transform.localRotation = Quaternion.Lerp(thisCam.transform.localRotation, Quaternion.Euler(new Vector3 (0f, Mathf.Clamp(playerVelocity.x * 1f, -25f, 25f), 0f)), Time.deltaTime * 3f);

	}

	void WHEEL(){

		if (useOcclusion && Occluding (playerCar.transform.position))
			ChangeCamera (CameraMode.TPS);

	}

	void TPS(){

		if (lastDirection != playerCar.direction) {

			direction = playerCar.direction;
			orbitX = 0f;
			orbitY = 0f;

		}

		lastDirection = playerCar.direction;

        // Рассчитать текущие углы поворота для режима TPS.
        wantedRotation = playerCar.transform.rotation * Quaternion.AngleAxis ((direction == 1 ? 0 : 180) + (useOrbitInTPSCameraMode ? orbitX : 0), Vector3.up);
		wantedRotation = wantedRotation * Quaternion.AngleAxis ((useOrbitInTPSCameraMode ? orbitY : 0), Vector3.right);

		if(Input.GetKey(KeyCode.B))
			wantedRotation = wantedRotation * Quaternion.AngleAxis ((180), Vector3.up);

        // Требуемая высота.
        wantedHeight = playerCar.transform.position.y + TPSHeight;

        // Уменьшить высоту.
        currentHeight = Mathf.Lerp (currentHeight, wantedHeight, TPSHeightDamping * Time.fixedDeltaTime);

        // Демпфировать вращение вокруг оси Y.
        if (Time.time > 1)
			currentRotation = Quaternion.Lerp(currentRotation, wantedRotation, TPSRotationDamping * Time.deltaTime);
		else
			currentRotation = wantedRotation;

        // Поворот камеры по оси Z для эффекта наклона.
        TPSTiltAngle = Mathf.Lerp(0f, TPSTiltMaximum * Mathf.Clamp(-playerVelocity.x, -1f, 1f), Mathf.Abs(playerVelocity.x) / 50f);
		TPSTiltAngle *= TPSTiltMultiplier;

        // Устанавливаем положение камеры на плоскости x-z для расстояния в метрах от цели.
        targetPosition = playerCar.transform.position;
		targetPosition -= (currentRotation) * Vector3.forward * (TPSDistance * Mathf.Lerp(1f, .75f, (playerRigid.velocity.magnitude * 3.6f) / 100f));
		targetPosition += Vector3.up * (TPSHeight * Mathf.Lerp(1f, .75f, (playerRigid.velocity.magnitude * 3.6f) / 100f));

        // Ровный
        // transform.position = SmoothApproach (pastFollowerPosition, pastTargetPosition, targetPosition, Mathf.Clamp (10f, Mathf.Abs (playerSpeed / 2f), Mathf.Infinity));
        // RAW.
        transform.position = targetPosition;

		thisCam.transform.localPosition = Vector3.Lerp(thisCam.transform.localPosition, new Vector3 (TPSTiltAngle / 10f, 0f, 0f), Time.deltaTime * 3f);

        // Всегда смотри на цель.
        transform.LookAt (playerCar.transform);
		transform.eulerAngles = new Vector3(currentRotation.eulerAngles.x + (TPSPitchAngle * Mathf.Lerp(1f, .75f, (playerRigid.velocity.magnitude * 3.6f) / 100f)), transform.eulerAngles.y, -Mathf.Clamp(TPSTiltAngle, -TPSTiltMaximum, TPSTiltMaximum) + TPSYawAngle);

        // Прошлые позиции, используемые для правильного сглаживания, связанные со скоростью.
        lastFollowerPosition = transform.position;
		lastTargetPosition = targetPosition;

        // Столкновение положений и поворотов, которые влияют на поворот камеры.
        collisionPos = Vector3.Lerp(new Vector3(collisionPos.x, collisionPos.y, collisionPos.z), Vector3.zero, Time.unscaledDeltaTime * 5f);

		if(Time.deltaTime != 0)
			collisionRot = Quaternion.Lerp(collisionRot, Quaternion.identity, Time.deltaTime * 5f);

        // Положение рычага и поворот оси до столкновения.
        pivot.transform.localPosition = Vector3.Lerp(pivot.transform.localPosition, collisionPos, Time.deltaTime * 10f);
		pivot.transform.localRotation = Quaternion.Lerp(pivot.transform.localRotation, collisionRot, Time.deltaTime * 10f);

        // Lerping targetFieldOfView от TPSMinimumFOV до TPSMaximumFOV, связанных со скоростью транспортного средства.
        targetFieldOfView = Mathf.Lerp(TPSMinimumFOV, TPSMaximumFOV, Mathf.Abs(playerSpeed) / 150f);

        // Синус FOV влияет на тяжелые аварии.
        targetFieldOfView += (5f * Mathf.Cos (index));

		if(useOcclusion)
			OccludeRay (playerCar.transform.position);

	}

	void FIXED(){

		if(fixedCam.transform.parent != null)
			fixedCam.transform.SetParent(null);

		if (useOcclusion && Occluding (playerCar.transform.position))
			fixedCam.ChangePosition ();

	}

	void TOP(){

        // Рано, если у нас нет машины игрока.
        if (!playerCar || !playerRigid)
			return;

        // Установка правильного targetPosition для режима верхней камеры.
        targetPosition = playerCar.transform.position;
		targetPosition += playerCar.transform.rotation * Vector3.forward * distanceOffset;

        // Настройка орто или перспективы?
        thisCam.orthographic = useOrthoForTopCamera;

		distanceOffset = Mathf.Lerp (0f, maximumZDistanceOffset, Mathf.Abs(playerSpeed) / 100f);
		targetFieldOfView = Mathf.Lerp (minimumOrtSize, maximumOrtSize, Mathf.Abs(playerSpeed) / 100f);
		thisCam.orthographicSize = targetFieldOfView;

        // Назначение положения и поворота.
        transform.position = SmoothApproach(lastFollowerPosition, lastTargetPosition, targetPosition, Mathf.Clamp(.1f, Mathf.Abs(playerSpeed / 2f), Mathf.Infinity));
		transform.rotation = Quaternion.Euler (topCameraAngle);

        // Прошлые позиции, используемые для правильного сглаживания, связанные со скоростью.
        lastFollowerPosition = transform.position;
		lastTargetPosition = targetPosition;

        // Поворотная позиция.
        pivot.transform.localPosition = new Vector3 (0f, 0f, -topCameraDistance);

	}

	void ORBIT(){

		// Зажимаем Y.
		orbitY = Mathf.Clamp(orbitY, minOrbitY, maxOrbitY);

		orbitRotation = Quaternion.Euler(orbitY, orbitX, 0f);

		if(playerSpeed > 10f && Mathf.Abs(orbitX) > 1f)
			orbitResetTimer += Time.deltaTime;

		if (playerSpeed > 10f && orbitResetTimer >= 2f) {

			orbitX = 0f;
			orbitY = 0f;
			orbitResetTimer = 0f;

		}

	}

	public void OnDrag(PointerEventData pointerData){

        // Получение перетаскивания из пользовательского интерфейса.
        orbitX += pointerData.delta.x * orbitXSpeed * .02f;
		orbitY -= pointerData.delta.y * orbitYSpeed * .02f;

		orbitResetTimer = 0f;

	}

	void CINEMATIC(){

		if(cinematicCam.transform.parent != null)
			cinematicCam.transform.SetParent(null);

		targetFieldOfView = cinematicCam.targetFOV;

		if (useOcclusion && Occluding (playerCar.transform.position))
			ChangeCamera (CameraMode.TPS);

	}

	public void Collision(Collision collision){

        // Если он не включен или режим камеры TPS, вернитесь.
        if (!enabled || !isRendering || cameraMode != CameraMode.TPS)
			return;

        // Локальная относительная скорость.
        Vector3 colRelVel = collision.relativeVelocity;
		colRelVel *= 1f - Mathf.Abs(Vector3.Dot(transform.up, collision.contacts[0].normal));

		float cos = Mathf.Abs(Vector3.Dot(collision.contacts[0].normal, colRelVel.normalized));

		if (colRelVel.magnitude * cos >= 5f){

			collisionVector = transform.InverseTransformDirection(colRelVel) / (30f);

			collisionPos -= collisionVector * 5f;
			collisionRot = Quaternion.Euler(new Vector3(-collisionVector.z * 10f, -collisionVector.y * 10f, -collisionVector.x * 10f));
			targetFieldOfView = thisCam.fieldOfView - Mathf.Clamp(collision.relativeVelocity.magnitude, 0f, 15f);
			index = Mathf.Clamp((colRelVel.magnitude * cos) * 50f, 0f, 10f);

		}

	}

	private void ResetCamera(){
		
		if(fixedCam)
			fixedCam.canTrackNow = false;

		TPSTiltAngle = 0f;

		collisionPos = Vector3.zero;
		collisionRot = Quaternion.identity;

		thisCam.transform.localPosition = Vector3.zero;
		thisCam.transform.localRotation = Quaternion.identity;

		pivot.transform.localPosition = collisionPos;
		pivot.transform.localRotation = collisionRot;

		lastFollowerPosition = transform.position;
		lastTargetPosition = targetPosition;

		orbitX = 0f;
		orbitY = 0f;

		thisCam.orthographic = false;

		switch (cameraMode) {

		case CameraMode.TPS:
			transform.SetParent(null);
			targetFieldOfView = TPSMaximumFOV;
			break;

		case CameraMode.FPS:
			transform.SetParent (hoodCam.transform, false);
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			targetFieldOfView = hoodCameraFOV;
			hoodCam.FixShake ();
			break;

		case CameraMode.WHEEL:
			transform.SetParent(wheelCam.transform, false);
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			targetFieldOfView = wheelCameraFOV;
			break;

		case CameraMode.FIXED:
			transform.SetParent(fixedCam.transform, false);
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			targetFieldOfView = 60;
			fixedCam.canTrackNow = true;
			break;

		case CameraMode.CINEMATIC:
			transform.SetParent(cinematicCam.pivot.transform, false);
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			targetFieldOfView = 30f;
			break;

		case CameraMode.TOP:
			transform.SetParent(null);
			targetFieldOfView = minimumOrtSize;
			pivot.transform.localPosition = Vector3.zero;
			pivot.transform.localRotation = Quaternion.identity;
			targetPosition = playerCar.transform.position;
			targetPosition += playerCar.transform.rotation * Vector3.forward * distanceOffset;
			transform.position = playerCar.transform.position;
			lastFollowerPosition = playerCar.transform.position;
			lastTargetPosition = targetPosition;
			break;

		}

	}

    // Используется для плавного позиционирования.
    private Vector3 SmoothApproach(Vector3 pastPosition, Vector3 pastTargetPosition, Vector3 targetPosition, float delta){
		
		if(Time.timeScale == 0 || float.IsNaN(delta) || float.IsInfinity(delta) || delta == 0 || pastPosition == Vector3.zero || pastTargetPosition == Vector3.zero || targetPosition == Vector3.zero)
			return transform.position;

		float t = (Time.deltaTime * delta) + .00001f;
		Vector3 v = ( targetPosition - pastTargetPosition ) / t;
		Vector3 f = pastPosition - pastTargetPosition + v;
		Vector3 l = targetPosition - v + f * Mathf.Exp( -t );

		#if UNITY_2017_1_OR_NEWER
		if (l != Vector3.negativeInfinity && l != Vector3.positiveInfinity && l != Vector3.zero)
			return l;
		else
			return transform.position;
		#else
		return l;
		#endif

	}

	public void ToggleCamera(bool state){

        // Включение / отключение активности.
        isRendering = state;

	}

	void OccludeRay(Vector3 targetFollow){

        // объявляем новый хит raycast.
        RaycastHit wallHit = new RaycastHit();

		//linecast from your player (targetFollow) to your cameras mask (camMask) to find collisions.
//		if (Physics.Linecast (targetFollow, transform.position, out wallHit, ~(1 << LayerMask.NameToLayer(RCCSettingsInstance.RCCLayer)))) {
//			
//			if (!wallHit.collider.isTrigger && !wallHit.transform.IsChildOf (playerCar.transform)) {
//
//				//the x and z coordinates are pushed away from the wall by hit.normal.
//				//the y coordinate stays the same.
//				Vector3 occludedPosition = new Vector3 (wallHit.point.x + wallHit.normal.x * .2f, wallHit.point.y + wallHit.normal.y * .2f, wallHit.point.z + wallHit.normal.z * .2f);
//
//				transform.position = occludedPosition;
//
//			}
//
//		}

		if (Physics.Linecast (targetFollow, transform.position, out wallHit, occlusionLayerMask)) {

			if (!wallHit.collider.isTrigger && !wallHit.transform.IsChildOf (playerCar.transform)) {

                // координаты x и z отталкиваются от стены с помощью hit.normal.
                // координата y остается неизменной.

                Vector3 occludedPosition = new Vector3 (wallHit.point.x + wallHit.normal.x * .2f, wallHit.point.y + wallHit.normal.y * .2f, wallHit.point.z + wallHit.normal.z * .2f);

				transform.position = occludedPosition;

			}

		}

	}

	bool Occluding(Vector3 targetFollow){

        // объявляем новый хит raycast.
        RaycastHit wallHit = new RaycastHit();

        // linecast от вашего игрока (targetFollow) к маске вашей камеры (camMask), чтобы найти столкновения.
        if (Physics.Linecast (targetFollow, transform.position, out wallHit, ~(1 << LayerMask.NameToLayer(RCCSettingsInstance.RCCLayer)))) {

			if (!wallHit.collider.isTrigger && !wallHit.transform.IsChildOf (playerCar.transform))
				return true;

		}

		return false;

	}

	void OnDisable(){

		RCC_CarControllerV3.OnRCCPlayerCollision -= RCC_CarControllerV3_OnRCCPlayerCollision;

	}

}