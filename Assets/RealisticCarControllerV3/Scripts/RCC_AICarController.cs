//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2014 - 2017 BoneCracker Games
// http://www.bonecrackergames.com
// Buğra Özdoğanlar
//
//----------------------------------------------

using UnityEngine;
using UnityEngine.AI;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Контроллер AI RCC. Это не профессионально, но это делает работу. Следит за всеми путевыми точками или преследует целевой игровой объект.
/// </summary>
[AddComponentMenu("BoneCracker Games/Realistic Car Controller/AI/RCC AI Car Controller")]
public class RCC_AICarController : MonoBehaviour {

	internal RCC_CarControllerV3 carController;     // Главный RCC этого транспортного средства.
    private Rigidbody rigid;                                    // Твердое тело этого транспортного средства.

    public RCC_AIWaypointsContainer waypointsContainer; // Контейнер путевых точек.
    public int currentWaypoint = 0;                                         // Текущий индекс в контейнере путевых точек.
    public Transform targetChase;                                           // Целевой Gameobject для чеканки.
    public string targetTag = "Player";                                 // Поиск и поиск Gameobjects по тегам.

    // AI тип
    public AIType _AIType;
	public enum AIType {FollowWaypoints, ChaseTarget}

    // Raycast расстояния, используемые для обнаружения препятствий в передней части нашего автомобиля AI.
    public int wideRayLength = 20;
	public int tightRayLength = 20;
	public int sideRayLength = 3;
	public LayerMask obstacleLayers = -1;

	private float rayInput = 0f;                // Общий входной луч, на который влияют расстояния лучевой передачи.
    private bool  raycasting = false;       // Raycasts встречает препятствие сейчас?
    private float resetTime = 0f;               // Этот таймер использовался для принятия решения вернуться или нет после сбоя.

    // Входы рулевого управления, двигателя и тормоза. Подаст RCC_CarController с этими входами.
    private float steerInput = 0f;
	private float gasInput = 0f;
	private float brakeInput = 0f;

    // Ограничить скорость.
    public bool limitSpeed = false;
	public float maximumSpeed = 100f;

    // Сглаженное рулевое управление.
    public bool smoothedSteer = true;

    // Тормозная зона.
    private float maximumSpeedInBrakeZone = 0f;
	private bool inBrakeZone = false;

    // Подсчитывает круги и сколько пройденных точек.
    public int lap = 0;
	public int totalWaypointPassed = 0;
	public int nextWaypointPassRadius = 40;
	public bool ignoreWaypointNow = false;

    // Unity's Navigator.
    private NavMeshAgent navigator;

    // Детектор со сферическим коллайдером. Используется для поиска целевых игровых объектов в режиме погони.
    private GameObject detector;
	public float detectorRadius = 100f;
	public List<RCC_CarControllerV3> targetsInZone = new List<RCC_CarControllerV3> ();

    // Запускаем событие, когда каждый автомобиль RCC AI появляется / активируется.
    public delegate void onRCCAISpawned(RCC_AICarController RCCAI);
	public static event onRCCAISpawned OnRCCAISpawned;

    // Запуск события, когда каждое транспортное средство AI RCC отключено / уничтожено.
    public delegate void onRCCAIDestroyed(RCC_AICarController RCCAI);
	public static event onRCCAIDestroyed OnRCCAIDestroyed;

	void Start() {

        // Получение основного контроллера.
        carController = GetComponent<RCC_CarControllerV3>();
		carController.externalController = true;

        // Получение основного Rigidbody.
        rigid = GetComponent<Rigidbody>();

        // Если на панели инспекторов контейнер путевых точек не выбран, найдите его на сцене.
        if (!waypointsContainer)
			waypointsContainer = FindObjectOfType(typeof(RCC_AIWaypointsContainer)) as RCC_AIWaypointsContainer;

        // Создание нашего навигатора и настройка свойств.
        GameObject navigatorObject = new GameObject("Navigator");
		navigatorObject.transform.SetParent (transform, false);
		navigator = navigatorObject.AddComponent<NavMeshAgent>();
		navigator.radius = 1;
		navigator.speed = 1;
		navigator.angularSpeed = 100000f;
		navigator.acceleration = 100000f;
		navigator.height = 1;
		navigator.avoidancePriority = 99;

        // Создание нашего детектора и настройка свойств. Используется для получения ближайших целевых игровых объектов.
        detector = new GameObject ("Detector");
		detector.transform.SetParent (transform, false);
		detector.gameObject.AddComponent<SphereCollider> ();
		detector.GetComponent<SphereCollider> ().isTrigger = true;
		detector.GetComponent<SphereCollider> ().radius = detectorRadius;

	}

	void OnEnable(){

        // Вызываем это событие, когда ИИ появляется.
        if (OnRCCAISpawned != null)
			OnRCCAISpawned (this);

	}
	
	void Update(){

        // Если не управляем, нет необходимости идти дальше.
        if (!carController.canControl)
			return;

        // Назначение позиции навигатора передним колесам автомобиля.
        //navigator.transform.localPosition = new Vector3 (0f, carController.FrontLeftWheelCollider.transform.localPosition.y, carController.FrontLeftWheelCollider.transform.localPosition.z);
        navigator.transform.position = transform.position;
		navigator.transform.position += transform.forward * carController.FrontLeftWheelCollider.transform.localPosition.z;

        // Удаление ненужных целей в списке.
        for (int i = 0; i < targetsInZone.Count; i++) {

			if(targetsInZone [i] == null)
				targetsInZone.RemoveAt (i);

			if (!targetsInZone [i].gameObject.activeInHierarchy)
				targetsInZone.RemoveAt (i);
			else {

				if(Vector3.Distance(transform.position, targetsInZone[i].transform.position) > (detectorRadius * 1.25f))
					targetsInZone.RemoveAt (i);

			}

		}

        // Если есть цель, найдите ближайшего врага.
        if (targetsInZone.Count > 0)
			targetChase = GetClosestEnemy(targetsInZone.ToArray());
		else
			targetChase = null;

	}
	
	void FixedUpdate (){

        // Если не управляем, нет необходимости идти дальше.
        if (!carController.canControl)
			return;

		Navigation();           // Питает steerInput на основе навигатора.
        FixedRaycasts();    // Влияет на steerInput, если один из радиопередач обнаруживает фронт объекта нашего автомобиля ИИ.
        FeedRCC();          // Ленты motorInput.
        Resetting();            // Был использован для принятия решения вернуться или нет после сбоя.

    }
	
	void Navigation (){

        // Входные данные навигатора умножаются на 1,5f для быстрой реакции.
        float navigatorInput = Mathf.Clamp(transform.InverseTransformDirection(navigator.desiredVelocity).x * 1.5f, -1f, 1f);

		switch (_AIType) {

		case AIType.FollowWaypoints:

                // Если в нашей сцене нет контейнера Waypoint, возвращаемся с ошибкой.
                if (!waypointsContainer){

				Debug.LogError("Waypoints Container Couldn't Found!");
				Stop();
				return;

			}

                // Если в нашей сцене есть контейнер путевых точек, и у него нет путевых точек, вернемся с ошибкой.
                if (waypointsContainer && waypointsContainer.waypoints.Count < 1){

				Debug.LogError("Waypoints Container Doesn't Have Any Waypoints!");
				Stop();
				return;

			}

                // Следующая точка маршрута.
                Vector3 nextWaypointPosition = transform.InverseTransformPoint(new Vector3(waypointsContainer.waypoints[currentWaypoint].position.x, transform.position.y, waypointsContainer.waypoints[currentWaypoint].position.z));

                // Установка пункта назначения навигатора.
                if (navigator.isOnNavMesh)
				navigator.SetDestination (waypointsContainer.waypoints [currentWaypoint].position);

                // Проверяет расстояние до следующей путевой точки. Если оно меньше записанного значения, переходите к следующей путевой точке.
                if (nextWaypointPosition.magnitude < nextWaypointPassRadius) {

				currentWaypoint++;
				totalWaypointPassed++;

                    // Если все путевые точки были пройдены, устанавливает текущую путевую точку на первую путевую точку и увеличивает круг.
                    if (currentWaypoint >= waypointsContainer.waypoints.Count) {
					
					currentWaypoint = 0;
					lap++;

				}

			}

			break;

		case AIType.ChaseTarget:

                // Если в нашей сцене нет контейнера Waypoints, вернемся с ошибкой.
                if (!targetChase){

                    //Debug.LogError("Target Chase не найден! ");
                    Stop();
				return;
	
			}

                // Установка пункта назначения навигатора.
                if (navigator.isOnNavMesh)
				navigator.SetDestination (targetChase.position);

			break;

		}

        // Ввод газа.
        if (!inBrakeZone){

			if(carController.speed >= 10){

				if(!carController.changingGear)
					gasInput = Mathf.Clamp(1f - (Mathf.Abs(navigatorInput / 10f)  - Mathf.Abs(rayInput / 10f)), .75f, 1f);
				else
					gasInput = 0f;

			}else{

				if(!carController.changingGear)
					gasInput = 1f;
				else
					gasInput = 0f;

			}

		}else{

			if(!carController.changingGear)
				gasInput = Mathf.Lerp(1f, 0f, (carController.speed) / maximumSpeedInBrakeZone);
			else
				gasInput = 0f;

		}

        // Управляемый вход.
        steerInput = Mathf.Clamp((ignoreWaypointNow ? rayInput : navigatorInput + rayInput), -1f, 1f) * carController.direction;

        // Тормозной вход.
        if (!inBrakeZone){
			
			if(carController.speed >= 25)
				brakeInput = Mathf.Lerp(0f, .25f, (Mathf.Abs(steerInput)));
			else
				brakeInput = 0f;

		}else{
			
			brakeInput = Mathf.Lerp(0f, 1f, (carController.speed - maximumSpeedInBrakeZone) / maximumSpeedInBrakeZone);

		}
			
	}
		
	void Resetting (){

        // Если не удается двигаться вперед, переводит передачу в R.
        if (carController.speed <= 5 && transform.InverseTransformDirection(rigid.velocity).z < 1f)
			resetTime += Time.deltaTime;
		
		if(resetTime >= 2)
			carController.direction = -1;

		if(resetTime >= 4 || carController.speed >= 25){
			
			carController.direction = 1;
			resetTime = 0;

		}
		
	}
	
	void FixedRaycasts(){

        // Поворотная позиция луча.
        Vector3 pivotPos = transform.position;
		pivotPos += transform.forward * carController.FrontLeftWheelCollider.transform.localPosition.z;

		RaycastHit hit;
		
		// New bools effected by fixed raycasts.
		bool  tightTurn = false;
		bool  wideTurn = false;
		bool  sideTurn = false;
		bool  tightTurn1 = false;
		bool  wideTurn1 = false;
		bool  sideTurn1 = false;
		
		// New input steers effected by fixed raycasts.
		float newinputSteer1 = 0f;
		float newinputSteer2 = 0f;
		float newinputSteer3 = 0f;
		float newinputSteer4 = 0f;
		float newinputSteer5 = 0f;
		float newinputSteer6 = 0f;

        // Рисуем лучи.
        Debug.DrawRay (pivotPos, Quaternion.AngleAxis(25, transform.up) * transform.forward * wideRayLength, Color.white);
		Debug.DrawRay (pivotPos, Quaternion.AngleAxis(-25, transform.up) * transform.forward * wideRayLength, Color.white);
		
		Debug.DrawRay (pivotPos, Quaternion.AngleAxis(7, transform.up) * transform.forward * tightRayLength, Color.white);
		Debug.DrawRay (pivotPos, Quaternion.AngleAxis(-7, transform.up) * transform.forward * tightRayLength, Color.white);

		Debug.DrawRay (pivotPos, Quaternion.AngleAxis(90, transform.up) * transform.forward * sideRayLength, Color.white);
		Debug.DrawRay (pivotPos, Quaternion.AngleAxis(-90, transform.up) * transform.forward * sideRayLength, Color.white);

        // Широкие Raycasts.
        if (Physics.Raycast (pivotPos, Quaternion.AngleAxis(25, transform.up) * transform.forward, out hit, wideRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform) {
			
			Debug.DrawRay (pivotPos, Quaternion.AngleAxis(25, transform.up) * transform.forward * wideRayLength, Color.red);
			newinputSteer1 = Mathf.Lerp (-.5f, 0f, (hit.distance / wideRayLength));
			wideTurn = true;

		}
		
		else{
			
			newinputSteer1 = 0f;
			wideTurn = false;

		}
		
		if (Physics.Raycast (pivotPos, Quaternion.AngleAxis(-25, transform.up) * transform.forward, out hit, wideRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform) {
			
			Debug.DrawRay (pivotPos, Quaternion.AngleAxis(-25, transform.up) * transform.forward * wideRayLength, Color.red);
			newinputSteer4 = Mathf.Lerp (.5f, 0f, (hit.distance / wideRayLength));
			wideTurn1 = true;

		}else{
			
			newinputSteer4 = 0f;
			wideTurn1 = false;

		}

        // Tight Raycasts.
        if (Physics.Raycast (pivotPos, Quaternion.AngleAxis(7, transform.up) * transform.forward, out hit, tightRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform) {
			
			Debug.DrawRay (pivotPos, Quaternion.AngleAxis(7, transform.up) * transform.forward * tightRayLength , Color.red);
			newinputSteer3 = Mathf.Lerp (-1f, 0f, (hit.distance / tightRayLength));
			tightTurn = true;

		}else{
			
			newinputSteer3 = 0f;
			tightTurn = false;

		}
		
		if (Physics.Raycast (pivotPos, Quaternion.AngleAxis(-7, transform.up) * transform.forward, out hit, tightRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform) {
			
			Debug.DrawRay (pivotPos, Quaternion.AngleAxis(-7, transform.up) * transform.forward * tightRayLength, Color.red);
			newinputSteer2 = Mathf.Lerp (1f, 0f, (hit.distance / tightRayLength));
			tightTurn1 = true;

		}else{
			
			newinputSteer2 = 0f;
			tightTurn1 = false;

		}

        // Side Raycasts.
        if (Physics.Raycast (pivotPos, Quaternion.AngleAxis(90, transform.up) * transform.forward, out hit, sideRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform) {
			
			Debug.DrawRay (pivotPos, Quaternion.AngleAxis(90, transform.up) * transform.forward * sideRayLength , Color.red);
			newinputSteer5 = Mathf.Lerp (-1f, 0f, (hit.distance / sideRayLength));
			sideTurn = true;

		}else{
			
			newinputSteer5 = 0f;
			sideTurn = false;

		}
		
		if (Physics.Raycast (pivotPos, Quaternion.AngleAxis(-90, transform.up) * transform.forward, out hit, sideRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform) {
			
			Debug.DrawRay (pivotPos, Quaternion.AngleAxis(-90, transform.up) * transform.forward * sideRayLength, Color.red);
			newinputSteer6 = Mathf.Lerp (1f, 0f, (hit.distance / sideRayLength));
			sideTurn1 = true;

		}else{
			
			newinputSteer6 = 0f;
			sideTurn1 = false;

		}

        // Raycasts встречает препятствие сейчас?
        if (wideTurn || wideTurn1 || tightTurn || tightTurn1 || sideTurn || sideTurn1)
			raycasting = true;
		else
			raycasting = false;

        // Если raycast попадает в коллайдер, подайте rayInput.
        if (raycasting)
			rayInput = (newinputSteer1 + newinputSteer2 + newinputSteer3 + newinputSteer4 + newinputSteer5 + newinputSteer6);
		else
			rayInput = 0f;

        // Если rayInput слишком много, игнорировать ввод в навигаторе.
        if (raycasting && Mathf.Abs(rayInput) > .5f)
			ignoreWaypointNow = true;
		else
			ignoreWaypointNow = false;
		
	}

	void FeedRCC(){

        // Подача газа на вход RСС.
        if (carController.direction == 1){
			if(!limitSpeed){
				carController.gasInput = gasInput;
			}else{
				carController.gasInput = gasInput * Mathf.Clamp01(Mathf.Lerp(10f, 0f, (carController.speed) / maximumSpeed));
			}
		}else{
			carController.gasInput = 0f;
		}

        // Кормление рулевого питания RСС.
        if (smoothedSteer)
			carController.steerInput = Mathf.Lerp(carController.steerInput, steerInput, Time.deltaTime * 20f);
		else
			carController.steerInput = steerInput;

        // Подача тормозного ввода RСС.
        if (carController.direction == 1)
			carController.brakeInput = brakeInput;
		else
			carController.brakeInput = gasInput;

	}

	void Stop(){

		gasInput = 0f;
		steerInput = 0f;
		brakeInput = 1f;

	}
	
	void OnTriggerEnter (Collider col){

        // Проверка, находится ли транспортное средство в любой тормозной зоне на сцене.
        if (col.gameObject.GetComponent<RCC_AIBrakeZone>()){
			
			inBrakeZone = true;
			maximumSpeedInBrakeZone = col.gameObject.GetComponent<RCC_AIBrakeZone>().targetSpeed;

		}

		if(col.attachedRigidbody != null && col.gameObject.GetComponentInParent<RCC_CarControllerV3>() && col.gameObject.GetComponentInParent<RCC_CarControllerV3>().transform.CompareTag(targetTag)){
			
			if (!targetsInZone.Contains (col.gameObject.GetComponentInParent<RCC_CarControllerV3> ()))
				targetsInZone.Add (col.gameObject.GetComponentInParent<RCC_CarControllerV3> ());

		}

	}

	void OnTriggerExit (Collider col){

		if(col.gameObject.GetComponent<RCC_AIBrakeZone>()){
			
			inBrakeZone = false;
			maximumSpeedInBrakeZone = 0;

		}

	}

	Transform GetClosestEnemy (RCC_CarControllerV3[] enemies){

		Transform bestTarget = null;

		float closestDistanceSqr = Mathf.Infinity;
		Vector3 currentPosition = transform.position;

		foreach(RCC_CarControllerV3 potentialTarget in enemies){

			Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
			float dSqrToTarget = directionToTarget.sqrMagnitude;

			if(dSqrToTarget < closestDistanceSqr){

				closestDistanceSqr = dSqrToTarget;
				bestTarget = potentialTarget.transform;

			}

		}

		return bestTarget;

	}

	void OnDestroy(){

        // Вызываем это событие, когда машина ИИ уничтожена.
        if (OnRCCAIDestroyed != null)
			OnRCCAIDestroyed (this);

	}
	
}