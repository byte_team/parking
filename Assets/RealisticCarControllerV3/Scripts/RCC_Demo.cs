﻿//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2014 - 2017 BoneCracker Games
// http://www.bonecrackergames.com
// Buğra Özdoğanlar
//
//----------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
// Простой скрипт-менеджер для всех демонстрационных сцен. В нем есть множество вызываемых транспортных средств игроков, общедоступные методы, установка новых режимов поведения, перезапуск и выход из приложения.
/// </summary>
[AddComponentMenu("BoneCracker Games/Realistic Car Controller/UI/RCC Demo Manager")]
public class RCC_Demo : MonoBehaviour {

	[Header("Spawnable Vehicles")]
	public RCC_CarControllerV3[] selectableVehicles;

	internal int selectedVehicleIndex = 0;      // Целочисленное значение индекса, используемое для порождения нового транспортного средства.
    internal int selectedBehaviorIndex = 0;     // Целочисленное значение индекса, используемое для установки режима поведения.

    // Целочисленное значение индекса, используемое для порождения нового транспортного средства.
    public void SelectVehicle (int index) {

		selectedVehicleIndex = index;
	
	}

	public void Spawn () {

        // Последняя известная позиция и поворот последнего активного транспортного средства.
        Vector3 lastKnownPos = new Vector3();
		Quaternion lastKnownRot = new Quaternion();

        // Проверка наличия транспортного средства игрока на сцене.
        if (RCC_SceneManager.Instance.activePlayerVehicle){

			lastKnownPos = RCC_SceneManager.Instance.activePlayerVehicle.transform.position;
			lastKnownRot = RCC_SceneManager.Instance.activePlayerVehicle.transform.rotation;

		}

        // Если последняя известная позиция и поворот не назначены, будет использоваться позиция и поворот камеры.
        if (lastKnownPos == Vector3.zero){
			
			if(RCC_SceneManager.Instance.activePlayerCamera){
				
				lastKnownPos = RCC_SceneManager.Instance.activePlayerCamera.transform.position;
				lastKnownRot = RCC_SceneManager.Instance.activePlayerCamera.transform.rotation;

			}

		}

        // Нам не нужен угол поворота X и Z. Просто Y.
        lastKnownRot.x = 0f;
		lastKnownRot.z = 0f;

		RCC_CarControllerV3 lastVehicle = RCC_SceneManager.Instance.activePlayerVehicle;

#if BCG_ENTEREXIT

		BCG_EnterExitVehicle lastEnterExitVehicle;
		bool enterExitVehicleFound = false;

		if (lastVehicle) {

			lastEnterExitVehicle = lastVehicle.GetComponentInChildren<BCG_EnterExitVehicle> ();

			if(lastEnterExitVehicle && lastEnterExitVehicle.driver){

				enterExitVehicleFound = true;
				BCG_EnterExitManager.Instance.waitTime = 10f;
				lastEnterExitVehicle.driver.GetOut();

			}

		}

#endif

        // Если у нас на сцене есть управляемое транспортное средство, уничтожаем его.
        if (lastVehicle)
			Destroy(lastVehicle.gameObject);

        // Здесь мы создаем наш новый автомобиль.
        RCC.SpawnRCC(selectableVehicles[selectedVehicleIndex], lastKnownPos, lastKnownRot, true, true, true);
		 
		#if BCG_ENTEREXIT

		if(enterExitVehicleFound){

			lastEnterExitVehicle = null;

			lastEnterExitVehicle = RCC_SceneManager.Instance.activePlayerVehicle.GetComponentInChildren<BCG_EnterExitVehicle> ();

			if(!lastEnterExitVehicle){
				
				lastEnterExitVehicle = RCC_SceneManager.Instance.activePlayerVehicle.gameObject.AddComponent<BCG_EnterExitVehicle> ();

			}

			if(BCG_EnterExitManager.Instance.BCGCharacterPlayer.characterPlayer && lastEnterExitVehicle && lastEnterExitVehicle.driver == null){
				
				BCG_EnterExitManager.Instance.waitTime = 10f;
				BCG_EnterExitManager.Instance.BCGCharacterPlayer.characterPlayer.GetIn(lastEnterExitVehicle);

			}

		}
		
		#endif

	}

    // Целочисленное значение индекса, используемое для установки режима поведения.
    public void SelectBehavior(int index){

		selectedBehaviorIndex = index;

	}

    // Здесь мы устанавливаем новое выбранное поведение на соответствующее.
    public void InitBehavior(){

		switch(selectedBehaviorIndex){
		case 0:
			RCC_Settings.Instance.behaviorType = RCC_Settings.BehaviorType.Simulator;
			RestartScene();
			break;
		case 1:
			RCC_Settings.Instance.behaviorType = RCC_Settings.BehaviorType.Racing;
			RestartScene();
			break;
		case 2:
			RCC_Settings.Instance.behaviorType = RCC_Settings.BehaviorType.SemiArcade;
			RestartScene();
			break;
		case 3:
			RCC_Settings.Instance.behaviorType = RCC_Settings.BehaviorType.Drift;
			RestartScene();
			break;
		case 4:
			RCC_Settings.Instance.behaviorType = RCC_Settings.BehaviorType.Fun;
			RestartScene();
			break;
		case 5:
			RCC_Settings.Instance.behaviorType = RCC_Settings.BehaviorType.Custom;
			RestartScene();
			break;
		}

	}

    // Просто перезапускаем текущую сцену.
    public void RestartScene(){

		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);

	}

    // Просто выйдите из приложения. Не работает на редакторе.
    public void Quit(){

		Application.Quit();

	}

}
