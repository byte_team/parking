﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
/// API для создания экземпляров, регистрации новых автомобилей RCC и изменений во время выполнения.
///</summary>
public class RCC : MonoBehaviour {

    ///<summary>
    /// Создает префаб транспортного средства RCC с заданным положением, вращением, устанавливает его управляемость и состояние двигателя.
    ///</summary>
    public static RCC_CarControllerV3 SpawnRCC (RCC_CarControllerV3 vehiclePrefab, Vector3 position, Quaternion rotation, bool registerAsPlayerVehicle, bool isControllable, bool isEngineRunning) {

		RCC_CarControllerV3 spawnedRCC = (RCC_CarControllerV3)GameObject.Instantiate (vehiclePrefab, position, rotation);
		spawnedRCC.gameObject.SetActive (true);
		spawnedRCC.SetCanControl (isControllable);

		if(registerAsPlayerVehicle)
			RCC_SceneManager.Instance.RegisterPlayer (spawnedRCC);

		if (isEngineRunning)
			spawnedRCC.StartEngine (true);
		else
			spawnedRCC.KillEngine ();

		return spawnedRCC;

	}

    ///<summary>
    /// Регистрирует транспортное средство как транспортное средство игрока.
    ///</summary>
    public static void RegisterPlayerVehicle(RCC_CarControllerV3 vehicle){

		RCC_SceneManager.Instance.RegisterPlayer (vehicle);

	}

    ///<summary>
    /// Регистрирует транспортное средство как транспортное средство игрока.
    ///</summary>
    public static void RegisterPlayerVehicle(RCC_CarControllerV3 vehicle, bool isControllable){

		RCC_SceneManager.Instance.RegisterPlayer (vehicle, isControllable);

	}

    ///<summary>
    /// Регистрирует транспортное средство как транспортное средство игрока.
    ///</summary>
    public static void RegisterPlayerVehicle(RCC_CarControllerV3 vehicle, bool isControllable, bool engineState){

		RCC_SceneManager.Instance.RegisterPlayer (vehicle, isControllable, engineState);

	}

    ///<summary>
    /// Отменяет регистрацию транспортного средства игрока.
    ///</summary>
    public static void DeRegisterPlayerVehicle(){

		RCC_SceneManager.Instance.DeRegisterPlayer ();

	}

    ///<summary>
    /// Устанавливает управляемое состояние автомобиля.
    ///</summary>
    public static void SetControl(RCC_CarControllerV3 vehicle, bool isControllable){

		vehicle.SetCanControl (isControllable);

	}

    ///<summary>
    /// Устанавливает состояние двигателя автомобиля.
    ///</summary>
    public static void SetEngine(RCC_CarControllerV3 vehicle, bool engineState){

		if (engineState)
			vehicle.StartEngine ();
		else
			vehicle.KillEngine ();

	}

    ///<summary>
    /// Устанавливает тип мобильного контроллера.
    ///</summary>
    public static void SetMobileController(RCC_Settings.MobileController mobileController){

		RCC_Settings.Instance.mobileController = mobileController;

	}

    ///<summary>
    /// Устанавливает единицы.
    ///</summary>
    public static void SetUnits(){}

    ///<summary>
    /// Устанавливает автоматическую передачу.
    ///</summary>
    public static void SetAutomaticGear(){}

    ///<summary>
    /// Запускает / останавливает запись автомобиля игрока.
    ///</summary>
    public static void StartStopRecord(){

		RCC_SceneManager.Instance.recorder.Record ();

	}

    ///<summary>
    /// Запуск / остановка воспроизведения последней записи.
    ///</summary>
    public static void StartStopReplay(){

		RCC_SceneManager.Instance.recorder.Play ();

	}

    ///<summary>
    /// Останавливает запись / воспроизведение последней записи.
    ///</summary>
    public static void StopRecordReplay(){

		RCC_SceneManager.Instance.recorder.Stop ();

	}

}
