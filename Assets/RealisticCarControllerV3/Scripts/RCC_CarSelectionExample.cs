﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Простой пример менеджера того, как должна работать сцена выбора автомобиля.
/// </summary>
public class RCC_CarSelectionExample : MonoBehaviour {

	public RCC_CarControllerV3[] spawnableVehicles;         // Наш список вызываемых транспортных средств.
    private List<RCC_CarControllerV3> _spawnedVehicles = new List<RCC_CarControllerV3> ();      // Наш список появившихся машин. Не нужно создавать экземпляры одного и того же автомобиля снова и снова.

    public Transform spawnPosition;		// Spawn transform.
	public int selectedIndex = 0;           // Выбранный индекс автомобиля. Следующие и предыдущие кнопки влияют на это значение.

    public RCC_Camera RCCCamera;        // Включение / отключение сценария выбора камеры на RCC Camera, если выбран.

    void Start () {

		if(!RCCCamera)
			RCCCamera = GameObject.FindObjectOfType<RCC_Camera> ();

        // Сначала мы создаем экземпляры всех транспортных средств и сохраняем их в списке _spawnedVehicles.
        CreateVehicles();
		
	}

	private void CreateVehicles(){

		for (int i = 0; i < spawnableVehicles.Length; i++) {

            // Заспаунить автомобиль без контроля, без игрока и выключенного двигателя. Мы не хотим позволить игроку управлять транспортным средством в меню выбора.
            RCC_CarControllerV3 spawnedVehicle = RCC.SpawnRCC (spawnableVehicles[i], spawnPosition.position, spawnPosition.rotation, false, false, false);
            // Отключение появившейся машины. 
            spawnedVehicle.gameObject.SetActive (false);
            // Добавляем и храним его в списке _spawnedVehicles.
            _spawnedVehicles.Add (spawnedVehicle);

		}

        // Включение первого индекса автомобиля.
        _spawnedVehicles[selectedIndex].gameObject.SetActive (true);

        // Если выбрана камера RCC, она включит скрипт RCC_CameraCarSelection. Этот скрипт был использован для орбитальной камеры.
        if (RCCCamera) {

			if (RCCCamera.GetComponent<RCC_CameraCarSelection> ())
				RCCCamera.GetComponent<RCC_CameraCarSelection> ().enabled = true;

		}

	}

    // Увеличение выбранного индекса, отключение всех других транспортных средств, включение текущего выбранного транспортного средства.
    public void NextVehicle () {

		selectedIndex++;

        // Если индекс превышает максимум, вернуться к 0.
        if (selectedIndex > _spawnedVehicles.Count - 1)
			selectedIndex = 0;

        // Отключение всех транспортных средств.
        for (int i = 0; i < _spawnedVehicles.Count; i++)
			_spawnedVehicles [i].gameObject.SetActive (false);

        // И включение только выбранного транспортного средства.
        _spawnedVehicles[selectedIndex].gameObject.SetActive (true);
		
	}

    // Уменьшение выбранного индекса, отключение всех других транспортных средств, включение текущего выбранного транспортного средства.
    public void PreviousVehicle () {

		selectedIndex--;

        // Если индекс ниже 0, вернемся к максимуму.
        if (selectedIndex < 0)
			selectedIndex = _spawnedVehicles.Count - 1;

        // Отключение всех транспортных средств.
        for (int i = 0; i < _spawnedVehicles.Count; i++)
			_spawnedVehicles [i].gameObject.SetActive (false);

        // И включение только выбранного транспортного средства.
        _spawnedVehicles[selectedIndex].gameObject.SetActive (true);

	}

    // Регистрация появившегося транспортного средства в качестве транспортного средства игрока, позволяющего управлять им.
    public void SelectVehicle(){

        // Регистрирует транспортное средство как транспортное средство игрока.
        RCC.RegisterPlayerVehicle (_spawnedVehicles[selectedIndex]);

        // Запускает двигатель и включает управление при выборе.
        _spawnedVehicles[selectedIndex].StartEngine ();
		_spawnedVehicles [selectedIndex].SetCanControl(true);

        // Если выбрана камера RCC, она отключит скрипт RCC_CameraCarSelection. Этот скрипт был использован для орбитальной камеры.
        if (RCCCamera) {

			if (RCCCamera.GetComponent<RCC_CameraCarSelection> ())
				RCCCamera.GetComponent<RCC_CameraCarSelection> ().enabled = false;

		}

	}

    // Деактивирует выбранный автомобиль и возвращает к выбору автомобиля.
    public void DeSelectVehicle(){

        // Отменяет регистрацию автомобиля.
        RCC.DeRegisterPlayerVehicle ();

        // Сбрасывает положение и вращение.
        _spawnedVehicles[selectedIndex].transform.position = spawnPosition.position;
		_spawnedVehicles [selectedIndex].transform.rotation = spawnPosition.rotation;

        // Убивает двигатель и отключает управляемость.
        _spawnedVehicles[selectedIndex].KillEngine ();
		_spawnedVehicles [selectedIndex].SetCanControl(false);

        // Сбрасывает скорость автомобиля.
        _spawnedVehicles[selectedIndex].GetComponent<Rigidbody> ().ResetInertiaTensor ();
		_spawnedVehicles [selectedIndex].GetComponent<Rigidbody> ().velocity = Vector3.zero;
		_spawnedVehicles [selectedIndex].GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;

        // Если выбрана камера RCC, она включит скрипт RCC_CameraCarSelection. Этот скрипт был использован для орбитальной камеры.
        if (RCCCamera) {
			
			if (RCCCamera.GetComponent<RCC_CameraCarSelection> ())
				RCCCamera.GetComponent<RCC_CameraCarSelection> ().enabled = true;
			
		}

	}

}
