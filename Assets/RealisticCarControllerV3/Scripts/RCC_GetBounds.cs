﻿//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2014 - 2017 BoneCracker Games
// http://www.bonecrackergames.com
// Buğra Özdoğanlar
//
//----------------------------------------------

using UnityEngine;
using System.Collections;

/// <summary>
/// Получает общий размер привязанного игрового объекта.
/// </summary>
public class RCC_GetBounds : MonoBehaviour {
	
	public static Vector3 GetBoundsCenter(Transform obj){

        // получить экстент центра границ объекта, включая все дочерние средства визуализации,
        // но исключая частицы и следы, для эффекта масштабирования FOV.

        var renderers = obj.GetComponentsInChildren<Renderer>();

		Bounds bounds = new Bounds();
		bool initBounds = false;
		foreach (Renderer r in renderers){
			
			if (!((r is TrailRenderer) || (r is ParticleSystemRenderer))){
				
				if (!initBounds){
					
					initBounds = true;
					bounds = r.bounds;

				}else{
					
					bounds.Encapsulate(r.bounds);

				}

			}

		}

		Vector3 center = bounds.center;
		return center;

	}

	public static float MaxBoundsExtent(Transform obj){

        // получить максимальный размер границ объекта, включая все дочерние средства визуализации,
        // но исключая частицы и следы, для эффекта масштабирования FOV.

        var renderers = obj.GetComponentsInChildren<Renderer>();

		Bounds bounds = new Bounds();
		bool initBounds = false;
		foreach (Renderer r in renderers)
		{
			if (!((r is TrailRenderer) || (r is ParticleSystemRenderer)))
			{
				if (!initBounds)
				{
					initBounds = true;
					bounds = r.bounds;
				}
				else
				{
					bounds.Encapsulate(r.bounds);
				}
			}
		}

		float max = Mathf.Max(bounds.extents.x, bounds.extents.y, bounds.extents.z);
		return max;

	}

}
