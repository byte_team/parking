﻿//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2014 - 2017 BoneCracker Games
// http://www.bonecrackergames.com
// Buğra Özdoğanlar
//
//----------------------------------------------

using UnityEngine;
using System.Collections;

/// <summary>
/// Тормозные зоны предназначены для замедления AI машин. Если у вас есть крутой поворот на вашей сцене, вы можете просто использовать одну из этих тормозных зон. У него целевая скорость. AI будет приспосабливать свою скорость к этой целевой скорости, находясь в этой зоне торможения. Это просто.
/// /// </summary>
[AddComponentMenu("BoneCracker Games/Realistic Car Controller/AI/RCC AI Brake Zone")]
public class RCC_AIBrakeZone : MonoBehaviour {
	
	public float targetSpeed = 50;
	
}
