﻿//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2014 - 2017 BoneCracker Games
// http://www.bonecrackergames.com
// Buğra Özdoğanlar
//
//----------------------------------------------

using UnityEngine;
using System.Collections;

/// <summary>
/// Отслеживает транспортное средство игрока и сохраняет ориентацию для кинематографических углов. У него есть игровой объект, называемый «Animation Pivot». Этот основной объект имеет 3 анимации.
/// </summary>
[AddComponentMenu("BoneCracker Games/Realistic Car Controller/Camera/RCC Cinematic Camera")]
public class RCC_CinematicCamera : MonoBehaviour {

	public GameObject pivot;                // Анимация Pivot.
    private Vector3 targetPosition;     // Целевая позиция для отслеживания.
    public float targetFOV = 60f;       // Целевое поле зрения.

    void Start () {

        // Если на панели инспектора не выбран сводный элемент, создайте его.
        if (!pivot) {
			
			pivot = new GameObject ("Pivot");
			pivot.transform.SetParent (transform, false);
			pivot.transform.localPosition = Vector3.zero;
			pivot.transform.localRotation = Quaternion.identity;

		}
	
	}

	void Update () {

        // Если текущее транспортное средство - ноль, возврат.
        if (!RCC_SceneManager.Instance.activePlayerVehicle)
			return;

        // Плавно поворачивается к автомобилю.
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.eulerAngles.x, RCC_SceneManager.Instance.activePlayerVehicle.transform.eulerAngles.y + 180f, transform.eulerAngles.z), Time.deltaTime * 3f);

        // Расчет целевой позиции.
        targetPosition = RCC_SceneManager.Instance.activePlayerVehicle.transform.position;
		targetPosition -= transform.rotation * Vector3.forward * 10f;

        // Назначение transform.position для targetPosition.
        transform.position = targetPosition;

	}

}
